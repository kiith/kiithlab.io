pelican -s pelicanconf.py

* NEED INTERNET: Re-check if the CV looks awkward w/ sidebar
  a link to CV.
* NEED INTERNET: Re-validate, fix anything left except <tt>
* NEED INTERNET: Point defenestrate.eu at the gitlab site.
* v01d. no JS, calendar. OLD v01d SITE ON x220. 
  Old content will be preserved somewhere, linked from 'old content'
* Some sort of comments. ISSO. Need to host it somewhere (v01d)
* New Ultisnips posts, and link from mastodon. And *use* mastodon, possibly from
  a dedicated browser.
* Blog posts:
    WRITE SINGLE-DAY POSTS, SO THEY'RE NOT LEFT UNFINISHED. EVEN IF THEY'RE IMPERFECT.
    - VIM WORKSHOP
    - CPU FUTURE: x86
    - CPU FUTURE: ARM
    - POCKETABLE DEVICES
    - POWERFUL BOARDS
    - Packet capture and kernel bypass approaches
    - CRPGs?
* CV cleanup
