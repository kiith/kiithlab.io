#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Ferdinand Majerech'
SITENAME = u'Defenestrate!'
SITESUBTITLE = u'Code and stuff. Mostly code.'
SHOW_SITESUBTITLE_IN_HTML = True
SITEURL = '.'

PATH = 'content'


STATIC_PATHS  = ['posts', 'pages']
ARTICLE_PATHS = ['posts']
OUTPUT_PATH = 'public'
THEME = 'nice-blog'
THEME_COLOR = 'red'
SIDEBAR_DISPLAY = ['about', 'tags']
COLOR_SCHEME_CSS = 'github.css'
SIDEBAR_ABOUT = """
<p>
Hi! I'm Ferdinand Majerech, a software developer in Slovakia.
</p>
<p>
I play with network security on ARM devices at work and (try to) maintain a
few open source projects I started back in the day. I enjoy graphics programming,
performance optimization and learning new creative things that can be done
with computers.
</p>
<p>
To contact me, send mail to kiithsacmp at Google's mail.
See also the <a href="http://v01d.sk">v01d</a> hackerspace and the
<a href="http://ics.upjs.sk/ihra">IHRA</a> competition.
</p>
"""
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

TIMEZONE = 'Europe/Bratislava'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['neighbors']

# TODO add links here - mastodon, tilde, github
# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# TODO find out how this is used (there is an Atom link in generated result, but not here)
# Social widget
SOCIAL = (('github', 'https://github.com/kiith-sa'),
          ('gitlab', 'https://gitlab.com/kiith'),
          ('retweet', 'https://tiny.tilde.website/@kiith'))

DEFAULT_PAGINATION = None

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
