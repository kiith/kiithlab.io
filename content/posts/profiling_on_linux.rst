====================
Profiling (on Linux)
====================

:date: 2014-10-31 12:00
:category: Optimization
:tags: code, d, c/c++, performance


Recently I gave a talk about profiling and optimization at a `local hackerspace
<http://www.v01d.sk>`_. The talk is mostly oriented at C/C++ but is also useful for D and other
native languages. It gives a basic overview of various useful (not ``gprof``) profiling
tools on Linux as well as some optimizations possible with C++ (and other native langs),
and also contains a bunch of links to more advanced articles relevant when you need to get
the best performance from a machine.

The most important tools are ``callgrind`` (from ``valgrind`` suite) and ``perf``. On
Debian-family Linuxes these can be installed with::

   sudo apt-get install valgrind linux-tools-common

or::

   sudo apt-get install valgrind linux-tools-generic

* `Slides <http://defenestrate.eu/_static/profiling-slides/index.html>`_
* `Sample code <https://github.com/kiith-sa/profiling>`_
