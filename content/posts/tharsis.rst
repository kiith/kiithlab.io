Tharsis
=======

:date: 2013-11-26 12:00
:category: Tharsis
:tags: code, gamedev, entity_systems, tharsis


In the previous post I described my issues with a component-based entity system
I used when working on `ICE <icegame.nfshost.com>`_. The most important of these 
was probably the inability to split the entity system into multiple threads.

`Tharsis <https://github.com/kiith-sa/Tharsis>`_ is my attempt to design an
entity component framework without these limitations.  Tharsis is a stand-alone
project, not tied to any single game. Its main goals are:

* Simulation results should be independent of how the *processes* (Tharsis'
  equivalent for *systems* in entity frameworks such as Artemis) are ordered.

* Processes shoule be automatically assigned to separate threads (mostly)
  without explicit thread management by the programmer.

* Performance should scale with increasing amount of cores (as long there is
  a large enough number of processes).

* Efficient memory organization, keeping CPU cache in mind.

* Open source with liberal (Boost) licencing.

The `design <https://github.com/kiith-sa/Tharsis/blob/master/tharsis.rst>`_ of
Tharsis also has other advantages, such as type safety (utilizing compile-time
features of the D language), 
`MultiComponents <https://github.com/kiith-sa/Tharsis/blob/master/tharsis.rst#multicomponent-concept>`_ 
and serialization of entities.

I hope to write a series of posts describing Tharsis and its development as
I add new features. Meanwhile, if you need a D component entity system *right
now*, check out `ArtemisD <https://github.com/elvisxzhou/artemisd>`_.
