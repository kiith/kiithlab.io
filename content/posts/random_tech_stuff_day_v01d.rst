============================
Random Tech Stuff Day @ v01d
============================

:date: 2016-01-24 12:00
:category: v01d
:tags: v01d


We're planning an event for v01d members to 'mutually show off' what we're working on and
possibly to recruit new members. This event will consist of 4 talks/workshops by v01d
members concerning whatever they are working on at the moment.

For my part, I'll be talking about technical details of mechanical keyboards (including
two different keyboards for people to try out), people making their own keyboards and
keyboard-centric UIs (from Vim and inspired UIs to keyboard-heavy RTS UIs).

Basic outline of the event, as announced on the gamedev meetup a few days ago:

.. admonition:: Random Tech Stuff Day

   Saturday ``20.02.2016``, ``13:00-17:30`` @v01d (``Narodna Trieda 74``)

   * Ferdinand Majerech: **The Keyboard Life**

     Mechanical keyboards, the excentrics around them, and keyboard UIs

   * Radoslav Strobl: **DIY Desktop Environment**

     Building a desktop on Awesome; a Lua programmable Window Manager

   * Eduard Drusa: **AVR/embedded ghetto style**

     How I built my AVR stack from scratch, and how far it is
     from blinkenlights to a smart home controller

   * Lubomir Nagajda & Co.: **Synthetic speech TBD**

     Content depends on research progress

