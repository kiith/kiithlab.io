=========================
KE gamedev meetup; take 0
=========================

:date: 2015-10-25 12:00
:category: Gamedev
:tags: gamedev, v01d

Yesterday, we had the *zeroth* gamedev meetup in Košice, Slovakia; which was my first
attempt at 'organizing' such a thing (AKA: sending a bunch of emails and killing a lot of
spiders).

A major twist was making this attempt in a `hackerspace <http://v01d.sk>`_; Slovakia
doesn't have much of a 'hacker-gamedev' culture at this point.

The presence of both game developers and hackers in the same room has lead to a bunch of
awkward moments as well as unintended comedy. Overall I think this combination could be
interesting if maintained in future.  For one, I've noticed that hackers and gamedevs can
have a sustained conversation on two separate topics while staying under the impression of
discussing the same topic; This tends to eventually fall appart as the inconsistency
eventually becomes too evident.

The event had a bunch of unexpected issues that should be avoided in future:

* We've noticed a bit too late that v01d has no... 'coat hanging devices' on the brink of
  winter. Our awesome 4D chair has served as a temporary substitute, though not
  a particularly ergonomic one.

* Randomly buying a bunch of low-quality food resulted in a bunch of low-quality food
  being left unused. Need to decrease low-quality food consumption estimates.

A more major (and somewhat expected) issue was the lack of capacity of the new v01d space;
while an upgrade compared to the previous space, it was filled to capacity by 10-15
people. Fully equipping v01d (new chairs, some rearrangement) will *slightly* improve the
capacity, where ``slightly <= 5``. Anything more would likely lead to air quality issues
and just the plain 'stuffed in a tin can' effect.

We will need a larger space for the meetup eventually, where eventually is ASAP.
Universities are an option that would likely work, though may lead to some bureaucracy.
I will keep looking for alternatives.


Not much *has been done* on the meetup (no talks, demos, etc. as there was no pre-planned
content); two main results have been:

* Creation of the ``v01d gamedev`` FB group. This will reduce my direct participation as
  I also count among the more staunch FB non-users; this might be for the better as my
  intention is to act as a 'placeholder' only for whatever necessities are not covered by
  other participants; eventually I'd like to see these events be either mostly
  self-organized or professionally organized (the latter may be harder at the moment due
  to the size of our community).

* Tentative date of the next meetup: ``07.01.2015``. This may or may not change, though.

