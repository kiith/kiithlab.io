===================
KE gamedev meetup 1
===================

:date: 2016-01-24 12:00
:category: Gamedev
:tags: gamedev, v01d


This Thursday we've had the first 'official' gamedev meetup in `v01d <http://v01d.sk>`_,
after the 'zeroth' experiment last autumn.

My expectations of too few or too many people ended up being unsubstantiated; we had
a slightly larger attendance than last time, but we were 'just full', without being
cramped.  Mandatory registration of attendees would add some certainty here, but i'm
reluctant to introduce it, as walking in is the most 'user friendly' way to attend. We
still have a problem there as our substitute for a doorbell is 'knocking at the window
with a Snowden poster'.

We've started on time (not without technical problems, but those were solved ahead) with
Peter Adamondy's talk, which introduced `Triple Hill <http://www.triple-hill.com/>`_'s
recently released game **Clumzee**, and morphed through Triple Hill's game design
philosophy to a preview of concepts behind their current, as of yet unannounced project.

While the talk was a success, by the time it was over, we ran through most of time
expected for the entire meetup. I had to cut off discussion to give space to the next
guest/contributor, Martin Baco.  Communication with contributors is an area I need to work
on overall; I only mentioned time allocation once in months-long conversations with
contributors and I didn't sufficiently communicate some details, e.g.  Martin had expected
at first to show a completely different project on the assumption that the meetup was not
open to public.

Martin's 'talk' was no talk; it was a live human experiment where I took the role of the
guinea pig.  Fishcow has apparently spent a chunk of their time trying to create the 'Next
Weird Thing' in the footsteps of such hits as Goat Simulator. It is perhaps unfortunate
that the concept I played as a part of the experiment did not come to fruition; it was one
of those extremely frustrating games that do not let go, and it's 'physics so bad it was
good' might make for some interesting YouTube memes if noticed.

Overall, compared to the zeroth experimental meetup the first was a major success, even
though it was still far from perfect and there is still a lot of space for improvement.

-----------------------------
Takeaways for the next meetup
-----------------------------

* Explicitly work out the amount of time allocated for every talk/demo/etc.
  with guests/contributors.
* Make sure all contributors are aware in detail of the format, e.g. the
  fact that it's a public meetup.
* Non-mandatory registration of interest (e.g. on v01d's website) could 
  still be helpful, at least to get a lower bound on the number of attendees.
* Don't bother that much with making the space look clean beforehand...
  it won't look much better either way and there'll be a lot to clean 
  after, especially now in winter.

--------------
Future meetups
--------------

For future, especially assuming we get more people, we're going to need more space than
possible in v01d. Even with some creative arrangement (e.g. tabourettes in front
of higher chairs) we can probably only accomodate <25 people.

I'm hoping to move the meetup to `UPJS <http://upjs.sk>`_, where we should be able to get 
enough space for around 50 people worst-case. This will likely require some compromises,
e.g. earlier starting time as we'd be required to be done by ``19:00``.

My current plan for the near-future meetups is as follows:

* **19.** March: ~ **v01d**
* May: ~ **UPJS**
* July: ~ **v01d** (summer holidays)
* September: ~ **UPJS** (if possible; may be early)

Note: the dates at UPJS are *only* my plan; they're not secured yet and may move to v01d.
Everything may still change.
