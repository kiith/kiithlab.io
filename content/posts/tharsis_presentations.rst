=====================
Tharsis presentations
=====================

:date: 2014-03-21
:category: Tharsis
:tags: tharsis, code, gamedev, entity_systems

I'm well overdue with my next Tharsis update, which I've been planning to write
since 3 months ago or so. For now, here are two presentations I gave on Tharsis
recently.  The 
`first </_static/entity-systems-presentation/build/slides/index.html>`_
presentation explains the rationale for component-based entities, while the 
`second </_static/presentation-3-2014/build/slides/index.html>`_ concentrates on
Tharsis itself and its potential performance limitations.

Meanwhile, work on Tharsis is moving (slowly... but still moving). Currently
I'm working on an example Process and Component that would handle entity
spawning while allowing to modify the entity being spawned (e.g. by overriding
its components).  Processes and Components such as these will be packaged with
Tharsis as an optional library to cover most common use-cases for beginners
while advanced users will be able to write their own.
