========================
Intro to gamedev using D
========================

:date: 2014-11-15 12:00
:category: D
:tags: code, d, gamedev

After ascertaining that I'm not going to spend any more time on workshops or talks,
I ended up preparing a... workshop for a local `OSS weekend
<http://ossden.soit.sk/index.php/ossvikendmenu>`_ event.

The workshop is an introduction to game development in the D language, with a focus on
*making a game* (for people who never made a game) rather than trying to show off all the
features of D.

`Slides <http://defenestrate.eu/_static/ossvikend/intro-gamedev-d/slides/index.html#1>`_
I prepared for the worshop are not really *presentation slides*, but rather a thinly
veiled tutorial with code to copy and bullet-points instead of long-winded paragraphs.
We'll see if that makes any sense at all.
