==========================
Frame-based game profiling
==========================

:date: 2014-09-05 12:00
:category: Tharsis
:tags: code, tharsis.prof, performance, gamedev

To make `Tharsis <https://github.com/kiith-sa/tharsis-core>`_ multi-threaded, I need to be
able to balance the overhead of individual `processes
<https://github.com/kiith-sa/tharsis-core/blob/master/tharsis.rst#process-concept>`_
(called Systems in many other ECS frameworks) by moving them between threads.  To do this,
I need to quickly measure the time spent running each process on a frame-by-frame basis.

When working on `ICE <https://github.com/kiith-sa/ICE>`_, I wrote a simple frame-based
profiler that recorded exact time when entering and exiting important *zones* of code,
such as rendering or GUI.  I wanted to update this profiler for Tharsis, but it turned out
to be easier to rewrite it due to heavy use of globals (not friendly to threaded code) and
occasional slow allocations that distorted the output.

I also intended to write a blog post about it... but it got a bit too long so I divided it
into three shorter posts. This post is an introduction to what frame-based profiling even
is, or rather, what I mean by it. It's likely there's a more common term than *frame-based
profiling*, but I didn't come across it yet.

Other parts of this series:

* `Optimizing memory usage of a frame based profiler <{filename}./optimizing_memory_usage_of_a_frame_based_profiler.rst>`_
* `Frame based profiling with D ranges <{filename}./frame_based_profiling_with_d_ranges.rst>`_



-------------------------------------------------------
Main-stream profilers are not always helful (for games)
-------------------------------------------------------

Profilers such as `perf <https://perf.wiki.kernel.org/index.php/Tutorial>`_, `CodeXL
<http://developer.amd.com/tools-and-sdks/opencl-zone/codexl/>`_ and even
`Callgrind+KCacheGrind <http://kcachegrind.sourceforge.net/html/Home.html>`_ are great for
finding the slowest parts of your code *in average case*. With a good profiler you can
identify bottlenecks on source line or instruction level, count cache misses, branch
mispredictions and so on. This is awesome to improve the *average* performance of your
game... and pretty useless when you get that two-second heisenlag that only happens once
per hour.

With a conventional profiler, irregular spikes in overhead disappear as they are averaged
over time. To track down such spikes, we need a profiler capable of keeping track of
individual *frames*.

--------------------
Frame-based profiler
--------------------

A frame-based profiler records overhead for each frame separately. An external tool can't
really know what 'frame' means (especially if 'frame' is decoupled from rendering), so we
have to instrument our code manually.

Once we know the duration of each frame, we can determine when any unexpected overhead
happens. If we know how long various parts (*zones*) of each frame take, we can look at
*zones* of slowest frames to find offending code.

.. _zones:

-----
Zones
-----

A *zone* is a section of code to measure overhead of. Zones may contain child zones (e.g.
a "rendering" zone may contain "batch" sub-zones).  In D or C++ we can implement this with
a RAII type that records the precise time in its constructor and destructor. This can be
pretty easy to use:

.. code-block:: d

   auto profiler = new Profiler(new ubyte[1024 * 1024 * 16]);
   while(!done)
   {
       Zone frameZone = Zone(profiler, "frame");

       {
           Zone renderZone = Zone(profiler, "render");

           // ... rendering code ..

           // renderZone end
       }
       {
           Zone collisionZone = Zone(profiler, "collision");

           // ... collision code ..

           // collisionZone end
       }
       {
           Zone aiZone = Zone(profiler, "AI");

           // ... AI code ..

           // aiZone end
       }

       // frameZone end
   }

.. note::

   To get precise time, use a high resolution clock such as
   ``std.datetime.Clock.currStdTime`` in D (also see `C (POSIX)
   <http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/>`_, `C++11
   <http://en.cppreference.com/w/cpp/chrono/high_resolution_clock>`_)


-----
Notes
-----

Unlike a conventional profiler, a frame-based profiler can't measure each function, line
or instruction.

Frame-based profiling doesn't *replace* a profiler; however, it can detect issues such
a profiler could not. Once you've localized the issue, you may even isolate it and use
e.g. a sampling profiler to optimize it.

Note that keeping track of profiling data can be pretty memory intensive. The next post is
about how I minimized the overhead of the frame profiler I wrote for Tharsis.
