=========================
Making graphics for games
=========================

:date: 2015-10-06 12:00
:category: Gamedev
:tags: 3d, gamedev, upjs


Apparently, this blog has been dead for a while.

This was caused mainly by my work on my `thesis project
<https://github.com/kiith-sa/tharsis-core>`_, which is now complete 
as far as the thesis is concerned (though the project still needs some work).

Right now, however, most of my free time is invested in working on what is supposed to be
an introductional course/subject into game development for `UPJS <http://upjs.sk>`_ (still
not set in stone), consisting of a number of high-level *independent* talks/workshops that
can be given in any order.

To ensure the talks are at least a bit non-crap I'm going to 'test' some of them in local
events. The first such test has occured a week ago in the `v01d <http://v01d.sk>`_
hackerspace.

As usual, the `slides <gamedev.upjs.sk/making-graphics>`_ are both presentation slides and
tutorial material to be viewed by the student.
