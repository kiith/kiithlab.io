===
CV
===

:date: 2017-01-29
:slug: cv

------------------
Ferdinand Majerech
------------------

* **Email**:  kiithsacmp@gmail.com
* **GitLab**: https://gitlab.com/kiith
* **GitHub**: https://github.com/kiith-sa
* **Blog**:   http://defenestrate.eu

------------------------------
Previous employment & projects
------------------------------

^^^^
Code
^^^^

.. XXX XXX modify HTML or CSS to make this more compact, hide-by-default less
           important things, etc.

* Spring 2016 - Spring 2017: **Software Engineer: Network Security** at **ESET**

  - Low-level network programming - TCP/IP stack, packet capture (C++11/Linux/ARM)

* Summer 2015 - Spring 2016: **Various C/C++ SH4/ARM projects** at **Antik Technology**

  - Smart home control platform (C++11/SH4)
  - Transcoder unit (Python, C++11/ARMv7)
  - OTA-update through MPEG/TS DSM-CC data carousels (C99/SH4)
  - other, minor projects

* June 2013 - November 2013: **A graph database system at SAGE team, s.r.o.**

  Was intended to be the backbone of an unconventional web framework. Written in C++.
  Project aborted due to investor departure.

* Other:

  - Various small D libraries
  - Various unreleased C++ game projects

^^^^^^^^
Non-code
^^^^^^^^

* **IHRA**: http://web.ics.upjs.sk/ihra

  Website, communication with game developers, secondary organization tasks; led 
  by Lubomir Antoni

* **v01d hackerspace**: http://v01d.sk

  Helping maintain the space, occasional talks & projects, as part of the local hacker
  community


------
Skills
------

* Human languages:

  - Slovak/Czech (fluent)
  - English      (fluent)

* Programming languages:

  - Bash             (beginner)
  - C                (intermediate)
  - C++ (incl. STL)  (advanced)
  - D                (advanced)
  - Python           (intermediate)
  - Other            (GLSL, x86 ASM, Java, SQL, Pascal, PHP, Vala ...)

**Tools**

* Linux development and administration

  - ssh
  - tmux
  - Experience with Debian, Mint, Ubuntu, OpenSUSE, Puppy, OpenWRT ...

* Any IDE/editor (Experience with Code::Blocks, Qt Creator, KDevelop, Eclipse...)

  - Vim with a custom plugin configuration preferred

* Any VCS (git, bzr, svn, etc.)

  - Git preferred
  - Feature branch workflow in Git and other DVCS's
  - Continous integration (used Travis CI)
  - Bisect VCS history to find bug sources (e.g. ``git bisect``)

* Compilers

  - GCC
  - LLVM/Clang

* Any issue tracking (RedMine, JIRA, Trac, GitHub, Gitorious...)

* Documentation

  - Doxygen, DDoc, JavaDoc, etc.
  - Sphinx/ReStructuredText, Markdown, etc.
  - LaTeX

* Debugging

  - GDB
  - valgrind: memcheck
  - strace/ltrace

* Profiling

  - perf
  - valgrind: cachegrind, callgrind, massif
  - oprofile
  - APITrace (for OpenGL)

**Code quality**

* Documenting code before commit

* Performance/Optimization

  - Algorithms and data structures (complexity)
  - Statistical and instrumentation-based profiling
  - Machine architecture understanding (cache performance, branch prediction, etc.)
  - Roll-your-own profiling (high precision clocks, custom allocators)

* Debugging

  - Finding memory leaks, uninitialized data usage with valgrind
  - Debugging syscall/library usage with strace/ltrace
  - Traditional step-by-step debugging
  - Small commits + binary searching VCS history (e.g. git bisect)

* Defensive programming

  - Contracts & invariants
  - Unittests
  - Const correctness, functional purity


**Domain-specific**

* Embedded with Linux (beginner)

  - ARM (v7, v8)
  - SH4

* Crossplatform libraries/frameworks (intermediate)

  - Boost
  - Qt
  - SDL
  - NCurses
  - GTK/Vala

* Graphics programming (advanced)

  - OpenGL 1-4.x
  - OpenGL ES 2.x

* Low-level network programming (intermediate) 

  - TCP/IP stack programming
  - Packet capture: NFQUEUE, libpcap, AF_PACKET, (beginner) DPDK

.. * Multimedia (MPEG standards, DVB)

---------
Interests
---------

* See *Skills*
* 3D graphics
* Literature, PC gaming, film
* Modding
* Astronomy, cosmonautics
* Cooking

---------
Education
---------

* 2013 - 2015:    Univezita Pavla Jozefa Šafárika v Košiciach
                  Faculty of Science
                  Informatics
                  Master's
* 2010 - 2013:    Univezita Pavla Jozefa Šafárika v Košiciach
                  Faculty of Science
                  Informatics
                  Bachelor's
* 2006 - 2010:    Gymnázium Školská 7, Spišská Nová Ves
