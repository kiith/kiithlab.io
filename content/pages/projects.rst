========
Projects
========

:date: 2017-03-26
:slug: projects

-------------------------------------------------
Open source projects (that I need to get back to)
-------------------------------------------------


* **Harbored-mod** (2014 - 2016)

  A documentation generator for the D programming language with DDoc and
  Markdown support. Based on `Harbored <https://github.com/economicmodeling/harbored>`_.

  `Source code <https://github.com/kiith-sa/harbored-mod>`__

* **Tharsis.prof** + **Despiker** (2014 - 2016)

  A real-time frame-based profiler (inspired by RAD Telemetry),
  profiling library and GUI (OpenGL + SDL) frontend.

  Posts on frame-based profiling:

  * `Frame based profiling <{filename}../posts/frame_based_game_profiling.rst>`_
  * `Optimizing memory usage of a frame based profiler <{filename}../posts/optimizing_memory_usage_of_a_frame_based_profiler.rst>`_
  * `Frame based profiling with D ranges <{filename}../posts/frame_based_profiling_with_d_ranges.rst>`_

  `Source code (Tharsis.prof) <https://github.com/kiith-sa/tharsis.prof>`_,
  `Source code (Despiker) <https://github.com/kiith-sa/despiker>`_

* **Tharsis** (2013 - 2016)

  Masters thesis project.
  An entity-component framework designed for automated threading of
  game/simulation logic.

  `Source code (tharsis-core) <https://github.com/kiith-sa/tharsis-core>`_,
  `Source code (tharsis-full) <https://github.com/kiith-sa/tharsis-full>`_

* **Awesome2D** (2012 - 2013)

  A 2D graphics engine showcasing advanced 2D lighting techniques.

  `Source code <https://github.com/kiith-sa/awesome2D>`__

* **ICE** (2010 - 2012)

  A moddable vertical shooter game written in D inspired by games such
  as Tyrian and Raptor.

  `Source code <https://github.com/kiith-sa/ICE>`__,
  `Site <http://icegame.nfshost.com/>`__,
  `Documentation <http://icegame.nfshost.com/doc/html/index.html>`__

* **D:YAML** (2011 - present)

  YAML parser and emitter written in D.

  `Source code <https://github.com/kiith-sa/D-YAML>`__,
  `Documentation <http://ddocs.org/dyaml/latest/index.html>`__

* **MiniINI** (2009 - 2010)

  A high-performance INI file parser in C++.  It was created as
  a challenge to create the fastest INI parser possible, and as such sacrifices
  API usability as well as code readability for performance.

  `Source code <http://bazaar.launchpad.net/~kiithsacmp/miniini/trunk/files>`__,
  `Site <http://miniini.tuxfamily.org/>`__,
  `Documentation <http://miniini.tuxfamily.org/docs.php>`__

-----------------
Talks & workshops
-----------------

* **Vim - the basics** (2017)

  Introductionary workshop for the vim text editor.

  `Notes <https://gitlab.com/kiith/slides/tree/master/vim-workshop-1>`__

* **Intro to development on ARM** (2016)

  Intro on how to choose an ARM board, performance characteristics of the ARM
  architecture, common pitfalls of development on ARM.

  `Slides <https://gitlab.com/kiith/slides/tree/master/ossvikend-arm>`__

* **The Keyboard Life** (2016)

  Talk about mechanical keyboards and keyboard user interfaces.

  `Slides <https://gitlab.com/kiith/slides/tree/master/keyboardlife>`__

* **Intro to game 3D graphics with Blender** (2015)

  An introduction to Blender and 3D modeling for games.

  `Slides and workshop source code
  <https://gitlab.com/kiith/slides/tree/master/ossvikend-blender>`__

* **Profiling (on Linux)** (2014)

  A talk/workshop about performance optimization in systems programming languages
  and an overview of the many different profiling tools available on Linux.

  `Slides and workshop source code <https://gitlab.com/kiith/slides/tree/master/profiling>`__


------
Random
------

`Old blender models <https://opengameart.org/users/kiith-sa>`_

.. image:: {attach}barrel.png
   :alt:   barrel
   :align: left
   :width: 45%
.. image:: {attach}tank.png
   :alt:   tank
   :align: right
   :width: 45%
